#QueryDSL
简书相关系列文章源码

#一、环境配置

###[第一章：Maven环境下如何配置QueryDSL环境](http://www.jianshu.com/p/a22447c0897c)

#二、基础

###[第二章：整合QueryDSL与SpringDataJPA](http://www.jianshu.com/p/4e9d8adaeeb2)

###[第三章：使用QueryDSL与SpringDataJPA完成Update&Delete](http://www.jianshu.com/p/ac388c3c36c2)

#三、提升

###[第四章：使用QueryDSL与SpringDataJPA实现多表关联查询](http://www.jianshu.com/p/6199e76a5485)

###[第五章：使用QueryDSL与SpringDataJPA实现查询返回自定义对象](http://www.jianshu.com/p/5c416a780b3e)

###[第六章：使用QueryDSL的聚合函数](http://www.jianshu.com/p/edfb4c2ff445)

###[第七章：使用QueryDSL与SpringDataJPA实现子查询](http://www.jianshu.com/p/a1d825a870af)